#ifndef PK_TYPES_H
#define PK_TYPES_H 1

#include "types.h"

enum pk_drug
{
	PK_DRUG_DALBA = 0,
	PK_DRUG_MAX
};

typedef union
{
	struct
	{
		float t;
		float c;
	};
	v2_t as_v2;
} pk_prel_t;

typedef struct
{
	float bsa; // m²
	float cl_creat; // mL/min
} pk_covar_t;

enum pk_struc_model
{
	PK_STRUC_MODEL_MONO = 0,
	PK_STRUC_MODEL_BI,
	PK_STRUC_MODEL_TRI,
	PK_STRUC_MODEL_MAX
};

typedef struct
{
	/* Compartment params */
	union
	{
		struct
		{
			float cl; // L/h
			float v; // L
		} comp[PK_STRUC_MODEL_MAX];
		v2_t comp0;
		v4_t comp01;
		v6_t comp012;
	};
	/* Specific for extravascular route */
	union
	{
		struct
		{
			float tlag;
			float ka;
		};
		v2_t ev;
	};
} pk_psi_t;

typedef struct
{
	enum pk_drug drug;
	pk_covar_t covar;
	enum pk_struc_model struc_model;
	v2_t res_model;
	pk_psi_t omega;
	pk_psi_t ppsi;
} pk_model_params_t;

enum pk_adm_route
{
	PK_ADM_ROUTE_IV_BOLUS = 0,
	PK_ADM_ROUTE_IV_PERF,
	PK_ADM_ROUTE_EV,
	PK_ADM_ROUTE_MAX
};

typedef struct
{
	enum pk_adm_route route;
	float t;
	/* Duration, needed for perf route */
	float dur;
	float dose;
} pk_adm_t;

typedef struct
{
	size_t adm_count;
	pk_adm_t *adms;
	bool is_eq;
} pk_adm_params_t;

static inline pk_psi_t pk_psi_set_monocomp(float cl0, float v0) {
	pk_psi_t r;

	r.comp[0].cl = cl0;
	r.comp[0].v = v0;
	return r;
}

static inline pk_psi_t pk_psi_set_monocomp_ev(float tlag, float ka, float cl0, float v0) {
	pk_psi_t r;

	r.tlag = tlag;
	r.ka = ka;
	r.comp[0].cl = cl0;
	r.comp[0].v = v0;
	return r;
}

static inline pk_psi_t pk_psi_set_bicomp(float cl0, float v0, float cl1, float v1) {
	pk_psi_t r;

	r.comp[0].cl = cl0;
	r.comp[0].v = v0;
	r.comp[1].cl = cl1;
	r.comp[1].v = v1;
	return r;
}

static inline pk_psi_t pk_psi_set_bicomp_ev(float tlag, float ka, float cl0, float v0, float cl1, float v1) {
	pk_psi_t r;

	r.tlag = tlag;
	r.ka = ka;
	r.comp[0].cl = cl0;
	r.comp[0].v = v0;
	r.comp[1].cl = cl1;
	r.comp[1].v = v1;
	return r;
}

static inline pk_psi_t pk_psi_set_tricomp(float cl0, float v0, float cl1, float v1, float cl2, float v2) {
	pk_psi_t r;

	r.comp[0].cl = cl0;
	r.comp[0].v = v0;
	r.comp[1].cl = cl1;
	r.comp[1].v = v1;
	r.comp[2].cl = cl2;
	r.comp[2].v = v2;
	return r;
}

static inline pk_psi_t pk_psi_set_tricomp_ev(float tlag, float ka, float cl0, float v0, float cl1, float v1, float cl2, float v2) {
	pk_psi_t r;

	r.tlag = tlag;
	r.ka = ka;
	r.comp[0].cl = cl0;
	r.comp[0].v = v0;
	r.comp[1].cl = cl1;
	r.comp[1].v = v1;
	r.comp[2].cl = cl2;
	r.comp[2].v = v2;
	return r;
}

extern const char *pk_adm_route_to_str(enum pk_adm_route route);
extern enum pk_adm_route pk_adm_route_from_str(const char *str);

#endif /* PK_TYPES_H */
