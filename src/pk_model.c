#include <math.h>

#include "pk_model.h"
#include "pk_types.h"
#include "random.h"
#include "util.h"

/***** Compute single adm concentration *****/

static float pk_model_compute_adm_conc_iv_bolus_bi(const pk_psi_t *psi, float t, float dur)
{
	(void)dur;

	float iv0 = 1.f / psi->comp[0].v;
	float iv1 = 1.f / psi->comp[1].v;
	float k10 = psi->comp[0].cl * iv0;
	float k12 = psi->comp[1].cl * iv0;
	float k21 = psi->comp[1].cl * iv1;
	float ks = k12 + k21 + k10;

	float sqrtdelta = sqrtf(ks * ks - 4.f * k21 * k10);
	float alpha = .5f * (-ks + sqrtdelta);
	float beta  = .5f * (-ks - sqrtdelta);
	float a =   alpha + k21;
	float b = -(beta  + k21);

	float cc = a * expf(alpha * t)
		     + b * expf(beta  * t);

	return cc * iv0 / (alpha - beta);
}

static float pk_model_compute_adm_conc_iv_perf_bi(const pk_psi_t *psi, float t, float dur)
{
	float iv0 = 1.f / psi->comp[0].v;
	float iv1 = 1.f / psi->comp[1].v;
	float k10 = psi->comp[0].cl * iv0;
	float k12 = psi->comp[1].cl * iv0;
	float k21 = psi->comp[1].cl * iv1;
	float ks = k12 + k21 + k10;

	float k20 = k21 * k10;
	float sqrtdelta = sqrtf(ks * ks - 4.f * k20);
	float beta = .5f * (ks - sqrtdelta);
	float alpha = k20 / beta;
	float a =   alpha - k21;
	float b = -(beta  - k21);
	float cc;

	if (t <= dur)
	{
		cc =  a / alpha * (1.f - expf(-alpha * t))
		    + b / beta  * (1.f - expf(-beta  * t));
	}
	else
	{
		t -= dur;
		cc = a / alpha * (1.f - expf(-alpha * dur)) * expf(-alpha * t)
		   + b / beta  * (1.f - expf(-beta  * dur)) * expf(-beta  * t);
	}

	return cc * iv0 / (alpha - beta);
}

static float pk_model_compute_adm_conc_ev_bi(const pk_psi_t *psi, float t, float dur)
{
	(void)dur;

	float iv0 = 1.f / psi->comp[0].v;
	float iv1 = 1.f / psi->comp[1].v;
	float k10 = psi->comp[0].cl * iv0;
	float k12 = psi->comp[1].cl * iv0;
	float k21 = psi->comp[1].cl * iv1;
	float ks = k12 + k21 + k10;

	float sqrtdelta = sqrtf(ks * ks - 4.f * k21 * k10);
	float beta  = .5f * (ks - sqrtdelta);
	float alpha = k21 * k10 / beta;
	float xx = psi->ka / (psi->comp[0].v * (beta - alpha));
	float a =  xx * (k21 - alpha) / (psi->ka - alpha);
	float b = -xx * (k21 - beta)  / (psi->ka - beta);

	float tt = max2f(t - psi->tlag, 0.f);
	float cc =  a      * expf(-alpha   * tt)
	         +  b      * expf(-beta    * tt)
	         - (a + b) * expf(-psi->ka * tt);

	return cc;
}

/*
 * psi: Simulation parameters
 * t:   Time of measure relative to administration
 * dur: Perf duration
 */
typedef float (*pk_model_cb_t)(const pk_psi_t *psi, float t, float dur);

enum pk_sim_eq
{
	PK_SIM_EQ_FALSE = 0,
	PK_SIM_EQ_TRUE = 1,
	PK_SIM_EQ_MAX
};

static const pk_model_cb_t model_callbacks[PK_STRUC_MODEL_MAX][PK_SIM_EQ_MAX][PK_ADM_ROUTE_MAX] = {
	[PK_STRUC_MODEL_BI] = {
		[PK_SIM_EQ_FALSE] = {
			[PK_ADM_ROUTE_IV_BOLUS] = pk_model_compute_adm_conc_iv_bolus_bi,
			[PK_ADM_ROUTE_IV_PERF] = pk_model_compute_adm_conc_iv_perf_bi,
			[PK_ADM_ROUTE_EV] = pk_model_compute_adm_conc_ev_bi
		}
	}
};

static inline float pk_model_compute_adm_conc(const pk_model_params_t *model, const pk_adm_params_t *ap, const pk_adm_t *adm, const pk_psi_t *psi, float t)
{
	enum pk_sim_eq eq = ap->is_eq ? PK_SIM_EQ_TRUE : PK_SIM_EQ_FALSE;
	pk_model_cb_t cb = model_callbacks[model->struc_model][eq][adm->route];

	return cb(psi, t - adm->t, adm->dur);
}

/***** Compute PSI *****/

static pk_psi_t pk_model_compute_psi_mono(const pk_model_params_t *model)
{
	pk_psi_t eta;
	pk_psi_t psi;

	eta.comp0 = v2_mul_v2(
			model->omega.comp0,
			v2_rand_normal_unsafe());
	psi.comp0 = v2_mul_v2(
			model->ppsi.comp0,
			v2_exp(eta.comp0));
	eta.ev = v2_mul_v2(
			model->omega.ev,
			v2_rand_normal_unsafe());
	psi.ev = v2_mul_v2(
			model->ppsi.ev,
			v2_exp(eta.ev));

	return psi;
}

static pk_psi_t pk_model_compute_psi_bi(const pk_model_params_t *model)
{
	pk_psi_t eta;
	pk_psi_t psi;

	eta.comp01 = v4_mul_v4(
			model->omega.comp01,
			v4_rand_normal_unsafe());
	psi.comp01 = v4_mul_v4(
			model->ppsi.comp01,
			v4_exp(eta.comp01));
	eta.ev = v2_mul_v2(
			model->omega.ev,
			v2_rand_normal_unsafe());
	psi.ev = v2_mul_v2(
			model->ppsi.ev,
			v2_exp(eta.ev));

	return psi;
}

static pk_psi_t pk_model_compute_psi_tri(const pk_model_params_t *model)
{
	pk_psi_t eta;
	pk_psi_t psi;

	eta.comp012 = v6_mul_v6(
			model->omega.comp012,
			v6_rand_normal_unsafe());
	psi.comp012 = v6_mul_v6(
			model->ppsi.comp012,
			v6_exp(eta.comp012));
	eta.ev = v2_mul_v2(
			model->omega.ev,
			v2_rand_normal_unsafe());
	psi.ev = v2_mul_v2(
			model->ppsi.ev,
			v2_exp(eta.ev));

	return psi;
}

typedef pk_psi_t (*pk_model_psi_cb_t)(const pk_model_params_t *model);

static const pk_model_psi_cb_t model_psi_callbacks[PK_STRUC_MODEL_MAX] = {
	[PK_STRUC_MODEL_MONO] = pk_model_compute_psi_mono,
	[PK_STRUC_MODEL_BI] = pk_model_compute_psi_bi,
	[PK_STRUC_MODEL_TRI] = pk_model_compute_psi_tri
};

static inline pk_psi_t pk_model_compute_psi(const pk_model_params_t *model)
{
	pk_model_psi_cb_t cb = model_psi_callbacks[model->struc_model];

	return cb(model);
}

/***** Compute final concentration *****/

float pk_model_compute_conc(const pk_model_params_t *model, const pk_adm_params_t *ap, float t)
{
	float cc = 0.f;
	pk_psi_t psi;

	psi = pk_model_compute_psi(model);

	for (size_t i = 0; i < ap->adm_count; i++)
	{
		const pk_adm_t *adm = &ap->adms[i];
		float c;

		if (adm->t > t)
			continue;

		c = pk_model_compute_adm_conc(model, ap, adm, &psi, t);

		c *= adm->dose;
		if (adm->route == PK_ADM_ROUTE_IV_PERF)
			c /= adm->dur;

		cc += c;
	}

	return cc;
}
