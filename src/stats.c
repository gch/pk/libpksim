#include <math.h>
#include <stdlib.h>

#include "stats.h"
#include "types.h"
#include "util.h"

static int cmp_float_cb(const void *pa, const void *pb)
{
	float a = *(float *)pa;
	float b = *(float *)pb;

	return (a > b) - (a < b);
}

void stats_samples_sort_asc(size_t n, float xs[])
{
	qsort(xs, n, sizeof(xs[0]), cmp_float_cb);
}

/* Needs a sorted array as input */
float stats_quantile_r7(float p, size_t n, const float xs[])
{
	p = clampf(0.f, p, 1.f);
	float h = (n - 1) * p;
	float hf = floorf(h);
	float hc = ceilf(h);
	float xf = xs[(size_t)hf];
	float xc;

	if (hf == hc)
		return xf;

	xc = xs[(size_t)hc];
	return xf + (h - hf) * (xc - xf);
}
