#ifndef LOADER_H
#define LOADER_H 1

#include "pk_types.h"

struct loader_context
{
	/* Params */
	bool has_params;
	size_t time_count;
	float time_interval;
	size_t sim_count;

	/* Patient */
	bool has_patient;
	char *lname;
	char *fname;
	float weight;
	float height;
	float bsa;
	float cl_creat;

	/* Adm */
	bool has_adm;
	pk_adm_params_t ap;

	/* Prel */
	bool has_prel;
	size_t prel_count;
	pk_prel_t *prels;
};

typedef int (*loader_launch_callback_t)(struct loader_context *ctx, void *arg);

extern int loader_process_file(FILE *fp, loader_launch_callback_t launch_cb, void *launch_cb_arg);

#endif /* LOADER_H */
