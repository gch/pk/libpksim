#ifndef STATS_H
#define STATS_H 1

#include <stddef.h>

extern void stats_samples_sort_asc(size_t n, float xs[]);
extern float stats_quantile_r7(float p, size_t n, const float xs[]);

#endif /* STATS_H */
