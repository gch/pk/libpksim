#include <stdlib.h>

#include "pk_drug_lib.h"
#include "pk_types.h"

static void pk_dalba_compute_model_params(pk_model_params_t *model, const pk_covar_t *covar)
{
	model->drug = PK_DRUG_DALBA;
	model->covar = *covar;
	model->struc_model = PK_STRUC_MODEL_BI;
	model->res_model = v2_set(0.f, 0.242f);

	model->omega = pk_psi_set_bicomp(0.18f, 0.245f, 0.746f, 0.296f);
	model->ppsi = pk_psi_set_bicomp(
			0.0571f + (covar->bsa - 2.05f) * 0.0109f + (covar->cl_creat - 120.6) * 0.000128f,
			4.15f   + (covar->bsa - 2.05f) * 2.70f,
			0.476f,
			11.4f);
}

void pk_compute_model_params(enum pk_drug drug, pk_model_params_t *model, const pk_covar_t *covar)
{
	switch (drug)
	{
		case PK_DRUG_DALBA: pk_dalba_compute_model_params(model, covar); break;
		default:
			break;
	}
}
