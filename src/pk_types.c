#include <string.h>

#include "pk_types.h"
#include "util.h"

#define ARR_ENUM(prefix, x) [x] = (&(#x)[sizeof(#prefix) - 1])

static const char *adm_routes[] = {
#define A_ENUM(x) ARR_ENUM(PK_ADM_ROUTE_, x)
	A_ENUM(PK_ADM_ROUTE_IV_BOLUS),
	A_ENUM(PK_ADM_ROUTE_IV_PERF),
	A_ENUM(PK_ADM_ROUTE_EV)
#undef A_ENUM
};

static inline int enum_from_str(size_t arr_len, const char *arr[], const char *str) {
	for (size_t i = 0; i < arr_len; i++)
	{
		const char *name = arr[i];
		if (!name)
			continue;

		if (strcmp(name, str) == 0)
			return i;
	}

	return -1;
}

const char *pk_adm_route_to_str(enum pk_adm_route route)
{
	if (route >= ARRAY_SIZE(adm_routes))
		return NULL;

	return adm_routes[route];
}

enum pk_adm_route pk_adm_route_from_str(const char *str)
{
	return enum_from_str(ARRAY_SIZE(adm_routes), adm_routes, str);
}
