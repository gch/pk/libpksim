#ifndef PK_MODEL_H
#define PK_MODEL_H 1

#include "pk_types.h"

extern float pk_model_compute_conc(const pk_model_params_t *model, const pk_adm_params_t *ap, float t);

#endif /* PK_MODEL_H */
